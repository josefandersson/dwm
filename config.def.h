/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx       = 2;        /* border pixel of windows */
static const int gappx                   = 5;        /* gap between windows */
static const int gapsingle               = 0;        /* gaps around lonely clients */
static const unsigned int snap           = 30;       /* snap pixel */
static const unsigned int systraypinning = 3;        /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 4;        /* systray spacing */
static const int systraypinningfailfirst = 1;        /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray             = 0;        /* 0 means no systray */
static const int showbar                 = 1;        /* 0 means no bar */
static const int topbar                  = 1;        /* 0 means bottom bar */
static const int focusonwheel            = 0;
static const char *fonts[]               = { "Fira Mono:size=9", "Noto Color Emoji:pixelsize=9:scale=13.5:weight=bold;2" };
// static const char col_gray1[]            = "#2a0f09";
// static const char col_gray2[]            = "#444444";
// static const char col_gray3[]            = "#bbbbbb";
// static const char col_gray4[]            = "#eeeeee";
// static const char col_cyan[]             = "#5d190e";
static char normbgcolor[]                = "#2a0f09";
static char normbordercolor[]            = "#444444";
static char normfgcolor[]                = "#bbbbbb";
static char selfgcolor[]                 = "#eeeeee";
static char selbordercolor[]             = "#5d190e";
static char selbgcolor[]                 = "#5d190e";
static char selmonfgcolor[]              = "#eeeeee";
static char selmonbordercolor[]          = "#5d190e";
static char selmonbgcolor[]              = "#5d190e";
static const unsigned int normbaralpha   = 0xff;
static const unsigned int selbaralpha    = 0xff;
static const unsigned int borderalpha    = 0xd0;
static const char *colors[][3]           = {
    /*               fg         bg         border   */
    [SchemeNorm]   = { normfgcolor, normbgcolor, normbordercolor },
    [SchemeSel]    = { selfgcolor,  selbgcolor,  selbordercolor  },
    [SchemeSelMon] = { selmonfgcolor,  selmonbgcolor,  selmonbordercolor  }, // when monitor is not selected... yes it's backwards...
};
static const unsigned int alphas[][3] = {
    /*               fg      bg        border      */
    [SchemeNorm]   = { OPAQUE, normbaralpha, borderalpha },
    [SchemeSel]    = { OPAQUE, selbaralpha, borderalpha },
    [SchemeSelMon] = { OPAQUE, selbaralpha, borderalpha },
};

/* autostart */
static char* const autostart[][7] = {
    { "nitrogen", "--restore", NULL },
    { "picom", NULL },
    { "imwheel", "--kill", NULL },
    { "nohup", "redshift", "-l", "57:12", "-t", "6300:4700", NULL },
    { "xset", "r", "rate", "195", "30", NULL },
    { "bash", "-c", "xinput --set-prop 8 'libinput Accel Speed' -0.7", NULL },
    { "/usr/bin/pidgin", "-c", "~/.config/purple", NULL },
    { "discord", NULL },
    { "pulseeffects", NULL },
    { "dwmblocks", NULL },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
    /* xprop(1):                               xprop | grep 'CLASS'
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class               instance  title  tags mask  switchtotag  isfloating   monitor   urgency */
    { "firefox",           NULL,     NULL,  1,         0,           0,          -1,       -1 },
    { "Pidgin",            NULL,     NULL,  1 << 4,    0,           0,           1,        1 },
    { "discord",           NULL,     NULL,  1 << 2,    0,           0,           0,        0 },
    { "Pulseeffects",      NULL,     NULL,  1 << 5,    0,           0,           1,       -1 },
    { "Spotify",           NULL,     NULL,  1 << 4,    0,           0,           0,       -1 },
    { "jetbrains-idea-ce", NULL,     NULL,  1,         0,           0,          -1,       -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
    /* symbol     arrange function */
    { "[]=",      tile },    /* first entry is default */
    { "><>",      NULL },    /* no layout function means floating behavior */
    { "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      comboview,      {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      combotag,       {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }, \
    { MODKEY|Mod1Mask|ControlMask,  KEY,      swaptags,       {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define BASHCMD(cmd) { .v = (const char*[]){ "/bin/bash", "-c", cmd, NULL } }

/* commands */
static const char *roficmd[]     = { "rofi", "-show", "run", NULL };
static const char *termcmd[]     = { "kitty", NULL };
static const char *browsercmd[]  = { "firefox", NULL };
static const char *mutevolcmd[]  = { "pactl", "set-sink-mute", "1", "toggle", NULL };
static const char *playcmd[]     = { "playerctl", "play-pause", NULL };
static const char *nextcmd[]     = { "playerctl", "next", NULL };
static const char *prevcmd[]     = { "playerctl", "previous", NULL };

static Key keys[] = {
    /* modifier           key                      function        argument */
    { MODKEY,             XK_space,                spawn,          {.v = roficmd } },
    { MODKEY|ShiftMask,   XK_Return,               spawn,          {.v = termcmd } },
    { MODKEY|ShiftMask,   XK_KP_Enter,             spawn,          {.v = termcmd } },
    { MODKEY|ControlMask, XK_l,                    spawn,          BASHCMD("~/.local/bin/i3lock-color-custom") },
    { MODKEY|ControlMask, XK_q,                    spawn,          BASHCMD("shutdown -h now") },
    { MODKEY,             XK_w,                    spawn,          {.v = browsercmd } },
    { 0,                  XK_Print,                spawn,          BASHCMD("~/.local/bin/screenshot -s -d 0.2") },
    { ShiftMask,          XK_Print,                spawn,          BASHCMD("~/.local/bin/screenshot") },
    { 0,                  XF86XK_AudioRaiseVolume, spawn,          BASHCMD("pactl set-sink-mute 1 false ; pactl set-sink-volume 1 +5% ; ~/.local/bin/statusbar/dwmblockssignal $(pgrep dwmblocks) 11 9") },
    { 0,                  XF86XK_AudioLowerVolume, spawn,          BASHCMD("pactl set-sink-mute 1 false ; pactl set-sink-volume 1 -5% ; ~/.local/bin/statusbar/dwmblockssignal $(pgrep dwmblocks) 11 9") },
    { 0,                  XF86XK_AudioMute,        spawn,          {.v = mutevolcmd } },
    { 0,                  XF86XK_AudioPlay,        spawn,          {.v = playcmd } },
    { 0,                  XF86XK_AudioNext,        spawn,          {.v = nextcmd } },
    { 0,                  XF86XK_AudioPrev,        spawn,          {.v = prevcmd } },
	{ 0,                  XF86XK_Calculator,       spawn,          BASHCMD("kitty -e bc -l") },
    { MODKEY|ShiftMask,   XK_b,                    togglegapsingle,{0} },
    { MODKEY,             XK_b,                    togglebar,      {0} },
    { MODKEY,             XK_j,                    focusstack,     {.i = +1 } },
    { MODKEY,             XK_k,                    focusstack,     {.i = -1 } },
    { MODKEY,             XK_i,                    incnmaster,     {.i = +1 } },
    { MODKEY,             XK_d,                    incnmaster,     {.i = -1 } },
    { MODKEY,             XK_h,                    setmfact,       {.f = -0.05} },
    { MODKEY,             XK_l,                    setmfact,       {.f = +0.05} },
    { MODKEY|ShiftMask,   XK_h,                    setcfact,       {.f = +0.25} },
    { MODKEY|ShiftMask,   XK_l,                    setcfact,       {.f = -0.25} },
    { MODKEY,             XK_Return,               zoom,           {0} },
    { MODKEY,             XK_Tab,                  view,           {0} },
    { MODKEY|ShiftMask,   XK_c,                    killclient,     {0} },
    { MODKEY,             XK_t,                    setlayout,      {.v = &layouts[0]} },
    { MODKEY,             XK_f,                    setlayout,      {.v = &layouts[1]} },
    { MODKEY,             XK_m,                    setlayout,      {.v = &layouts[2]} },
    { MODKEY|ShiftMask,   XK_space,                togglefloating, {0} },
    { MODKEY,             XK_s,                    togglesticky,   {0} },
    { MODKEY,             XK_0,                    view,           {.ui = ~0 } },
    { MODKEY|ShiftMask,   XK_0,                    tag,            {.ui = ~0 } },
    { MODKEY,             XK_comma,                focusmon,       {.i = -1 } },
    { MODKEY,             XK_period,               focusmon,       {.i = +1 } },
    { MODKEY|ShiftMask,   XK_comma,                tagmon,         {.i = -1 } },
    { MODKEY|ShiftMask,   XK_period,               tagmon,         {.i = +1 } },
    TAGKEYS(              XK_1,                                    0)
    TAGKEYS(              XK_2,                                    1)
    TAGKEYS(              XK_3,                                    2)
    TAGKEYS(              XK_4,                                    3)
    TAGKEYS(              XK_5,                                    4)
    TAGKEYS(              XK_6,                                    5)
    TAGKEYS(              XK_7,                                    6)
    TAGKEYS(              XK_8,                                    7)
    TAGKEYS(              XK_9,                                    8)
    { MODKEY|ShiftMask,   XK_q,                    quit,           {0} },
    { MODKEY,             XK_minus,                setgaps,        {.i = -4} },
    { MODKEY|ShiftMask,   XK_minus,                setgaps,        {.i =  0} },
    { MODKEY,             XK_plus,                 setgaps,        {.i = +4} },
    { MODKEY,             XK_F5,                   xrdb,           {.v = NULL } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,              Button1,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigdwmblocks,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigdwmblocks,   {.i = 5} },
	{ ClkStatusText,        ShiftMask,      Button1,        sigdwmblocks,   {.i = 6} },
	{ ClkStatusText,        ShiftMask,      Button2,        sigdwmblocks,   {.i = 7} },
	{ ClkStatusText,        ShiftMask,      Button3,        sigdwmblocks,   {.i = 8} },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkClientWin,         MODKEY,         Button4,        setgaps,        {.i = -4} },
    { ClkClientWin,         MODKEY,         Button5,        setgaps,        {.i = +4} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
    { ClkRootWin,           0,              Button2,        togglebar,      {0} },
    { ClkRootWin,           0,              Button3,        togglegapsingle,{0} },
};

